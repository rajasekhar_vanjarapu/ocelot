using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.IO;
using Ocelot.DependencyInjection;
using Serilog;
using Serilog.Events;
using Microsoft.Extensions.Configuration;
using Ocelot.Middleware;
using gateway.Middlewares;
using Microsoft.AspNetCore.Builder;

namespace gateway
{
    public class Program
    {
    //     public static void Main(string[] args)
    //     {
    //         //CreateHostBuilder(args).Build().Run();
    //         IWebHostBuilder builder = new WebHostBuilder();  
    //         builder.ConfigureServices(s =>  
    //         {  
    //             s.AddSingleton(builder);  
    //             s.AddOcelot();
    //         });  

    //         builder.UseSerilog((_, config) =>
    //         {
    //             config
    //                 .MinimumLevel.Information()
    //                 .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
    //                 .Enrich.FromLogContext()
    //                 .WriteTo.File(@"Logs\log.txt", rollingInterval: RollingInterval.Day);
    //         });

    //         //logging
    //         builder.ConfigureLogging(logBuilder => {
    //             logBuilder.ClearProviders();
    //             logBuilder.AddConsole();
    //             logBuilder.AddTraceSource("Information, ActivityTracing");
    //         });

    //         builder.UseKestrel()  
    //             .UseContentRoot(Directory.GetCurrentDirectory())  
    //             .UseStartup<Startup>();
    //             //.UseUrls("http://localhost:9000");  
    
    //         var host = builder.Build();  
    //         host.Run();  
    //     }

    //     public static IHostBuilder CreateHostBuilder(string[] args) =>
    //         Host.CreateDefaultBuilder(args)
    //             .ConfigureWebHostDefaults(webBuilder =>
    //             {
    //                 webBuilder.UseStartup<Startup>();
    //             });
    // }
    public static void Main(string[] args)
        {
            new WebHostBuilder()
               .UseKestrel()
               .UseContentRoot(Directory.GetCurrentDirectory())
               .ConfigureAppConfiguration((hostingContext, config) =>
               {
                   config
                       .SetBasePath(hostingContext.HostingEnvironment.ContentRootPath)
                       .AddJsonFile("appsettings.json", true, true)
                       .AddJsonFile($"appsettings.{hostingContext.HostingEnvironment.EnvironmentName}.json", true, true)
                       .AddJsonFile("ocelot.json")
                       .AddEnvironmentVariables();
               })
               .ConfigureServices(s => {
                   s.AddOcelot();
               })
               .UseSerilog((_, config) =>
                {
                    config
                        .MinimumLevel.Information()
                        .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                        .Enrich.FromLogContext()
                        .WriteTo.File(@"Logs\log.txt", rollingInterval: RollingInterval.Day);
                })
               .ConfigureLogging((hostingContext, logging) =>
               {
                    logging.ClearProviders();
                    logging.AddConsole();
                    logging.AddTraceSource("Information, ActivityTracing");
               })
               .UseIISIntegration()
               .Configure(app =>
               {
                   app
                   .UseMiddleware<LoggingMiddleware>()
                   .UseOcelot().Wait();
               })
               .Build()
               .Run();
        }
    }
}
